# main_app

This template should help get you started developing with Vue 3 in Vite.

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur).

## Type Support for `.vue` Imports in TS

TypeScript cannot handle type information for `.vue` imports by default, so we replace the `tsc` CLI with `vue-tsc` for type checking. In editors, we need [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) to make the TypeScript language service aware of `.vue` types.

## Customize configuration

1. 配置文件放在public目录下，config.json，里面可以配置服务端API地址，还有大量应用相关的信息，该配置档为静态文件，部署服务器后可以随时修改并生效，无需重新build

- 配置参数说明如下：

- ```
  {

   "$URL": {

    "CENTER_API": "API服务地址",

    "IMG_UPLOAD_URL": "该参数不用管",

    "REMOTE_FILE_BASE": "远程文件访问跟地址，匹配阿里云OSS的访问地址"

   },

   "$System": {

    "Title": "不用动",

    "Version": "不用动",

    "Copyright": "不用动",

    "ShowNotice": "不用动",

    "UseStaticMenu": "不用动",

    "PageSize": "不用动",

    "PageSizeOptions": "不用动",

    "VideoUploadMaxSize": "视频上传最大Size",

    "PicUploadMaxSize": "图片上传最大Size",

    "RouteMode": "不用动"

   },
  // 以下为阿里云OSS相关配置
   "AliyunOSS": {

    "region": "从阿里云OSS后台获取相关配置信息",

    "endpoint": "从阿里云OSS后台获取相关配置信息",

    "bucket": "OSS的桶名",

    "Path": "OSS上传文件存放根路径",

    "refreshTokenTime": "不用管"

   }

  }
  ```

1. .env.dev用来配置打包的base path，同时注意，该配置修改后，/src/common/config.ts中的const basePath = '/'也该跟着修改成对应的路径。
1. locale配置文件放在src/locales下，与其他locale方案不同，该locale直接用默认语言的内容作为key，比如中文为：欢迎使用，则en.ts中就配置一个

```
{
"欢迎使用": "Welcome"
}
```

即可

1. Axios的配置放在/src/axios/http.ts中，相关拦截和特殊处理在该文件中进行
1. API的开发目录为/src/api，开发人员在该目录下进行api的开发
1. 常用的工具方法放在/src/common中
1. 全局样式放在/src/styles/element中，var.css存放了全局的样式变量，里面分为默认模式和dark模式
1. richtextEditor：- [文档](https://www.wangeditor.com/)

## 相关问题说明

1. Vue3VideoPlay引入后运行是存在bug的，需要将node_modules 下vue3-video-play文件夹下 package.json文件做修改
   ![alt text](image.png)

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```
