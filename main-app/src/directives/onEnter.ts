import { App, Directive, DirectiveBinding } from 'vue'

let _e: Function
// 定义指令
const enter: Directive = {
  // 当被绑定的元素挂载到 DOM 上时调用
  mounted(el, binding) {
    _e = (event) => {
      // 检查是否是 Enter 键
      if (event.key === 'Enter') {
        // 如果提供了方法，则调用它
        if (typeof binding.value === 'function') {
          binding.value()
        }
      }
    }
    el.addEventListener('keydown', _e)
  },
  // 当绑定的元素从 DOM 上卸载时调用
  unmounted(el) {
    el.removeEventListener('keydown', _e)
  }
}

// 定义一个全局方法，用于注册指令
export function registerEnterDirective(app: App) {
  app.directive('enter', enter)
}
