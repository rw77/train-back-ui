import { ServerConfig, Sysinfo } from '@/common/config'
import { get, post, upload } from '@/axios/http'
import { t } from '@/common/locale'
import comFunc from '@/common/comFunc'
import { orderBy } from 'element-plus/es/components/table/src/util'

export const queryList = async (p: {
  categoryId?: string
  name?: string
  pageSize?: number
  to?: number
}): Promise<{
  isSuccess: boolean
  msg: string
  data?: any[]
  pageSize?: number
  pageNum?: number
  totalRow?: number
  totalPages?: number
}> => {
  if (!p.pageSize) p.pageSize = Sysinfo.PageSize
  if (!p.to) p.to = 1
  const re = await post('/project/course/getCourseByCondition', {
    categoryId: comFunc.nvl(p.categoryId) === '' ? '' : p.categoryId,
    name: comFunc.nvl(p.name) === '' ? '' : p.name,
    timeStatus: '',
    hideMyJoined: false,
    expertId: '',
    enable: null,
    pageNum: p.to,
    pageSize: p.pageSize,
    orderBy: 'CREATE_TIME'
  }).catch((err) => {
    return err.response || err
  })
  const data =
    re.data && re.data.data
      ? re.data.data
      : {
          list: [],
          pageSize: Sysinfo.PageSize,
          pageNum: 1,
          totalRow: 0,
          totalPages: 0
        }
  if (re.data) {
    if (re.data.success) {
      return {
        isSuccess: re.data.success,
        msg: re.data.message,
        data: data.list,
        pageSize: data.pageSize > 0 ? data.pageSize : Sysinfo.PageSize,
        pageNum: data.pageNum,
        totalRow: data.total,
        totalPages: data.pages
      }
    } else {
      return {
        isSuccess: re.data.success,
        msg: re.data.message
      }
    }
  } else {
    return {
      isSuccess: false,
      msg: `查询失败:无返回结果`
    }
  }
}
export const getDetail = async (
  id: string
): Promise<{
  isSuccess: boolean
  msg: string
  data?: CourseDataModel
}> => {
  const re = await get('/project/course/getCourseById?objectId=' + id).catch((err) => {
    return err.response || err
  })
  const data = re.data && re.data.data ? re.data.data : null
  if (re.data) {
    if (re.data.success) {
      return {
        isSuccess: re.data.success,
        msg: re.data.message,
        data: data
      }
    } else {
      return {
        isSuccess: re.data.success,
        msg: re.data.message
      }
    }
  } else {
    return {
      isSuccess: false,
      msg: `查询失败:无返回结果`
    }
  }
}
export const getUnrelatedCourseList = async (p: {
  categoryId?: string
  expertId?: string
  hideMyJoined?: boolean
  name?: string
  orderBy?: string
  pageNum?: number
  pageSize?: number
  pid?: string
  timeStatus?: string
}): Promise<{
  isSuccess: boolean
  msg: string
  data?: any[]
  pageSize?: number
  pageNum?: number
  totalRow?: number
  totalPages?: number
}> => {
  if (!p.pageSize) p.pageSize = Sysinfo.PageSize
  if (!p.pageNum) p.pageNum = 1
  const re = await post('/project/course/getUnrelatedCourseByExpertId', p).catch((err) => {
    return err.response || err
  })
  const data =
    re.data && re.data.data
      ? re.data.data
      : {
          list: [],
          pageSize: Sysinfo.PageSize,
          pageNum: 1,
          totalRow: 0,
          totalPages: 0
        }
  if (re.data) {
    if (re.data.success) {
      return {
        isSuccess: re.data.success,
        msg: re.data.message,
        data: data.list,
        pageSize: data.pageSize > 0 ? data.pageSize : Sysinfo.PageSize,
        pageNum: data.pageNum,
        totalRow: data.total,
        totalPages: data.pages
      }
    } else {
      return {
        isSuccess: re.data.success,
        msg: re.data.message
      }
    }
  } else {
    return {
      isSuccess: false,
      msg: `查询失败:无返回结果`
    }
  }
}
export const getCourseComments = async (p: {
  apprType?: string
  relatedId?: string
  pageSize?: number
  to?: number
}): Promise<{
  isSuccess: boolean
  msg: string
  data?: any[]
  pageSize?: number
  pageNum?: number
  totalRow?: number
  totalPages?: number
}> => {
  if (!p.pageSize) p.pageSize = Sysinfo.PageSize
  if (!p.to) p.to = 1
  const re = await post('/project/course/getCourseComment', p).catch((err) => {
    return err.response || err
  })
  const data =
    re.data && re.data.data
      ? re.data.data
      : {
          list: [],
          pageSize: Sysinfo.PageSize,
          pageNum: 1,
          totalRow: 0,
          totalPages: 0
        }
  if (re.data) {
    if (re.data.success) {
      return {
        isSuccess: re.data.success,
        msg: re.data.message,
        data: data.list,
        pageSize: data.pageSize > 0 ? data.pageSize : Sysinfo.PageSize,
        pageNum: data.pageNum,
        totalRow: data.total,
        totalPages: data.pages
      }
    } else {
      return {
        isSuccess: re.data.success,
        msg: re.data.message
      }
    }
  } else {
    return {
      isSuccess: false,
      msg: `查询失败:无返回结果`
    }
  }
}
export const approveComment = async (data: {
  approveType: string
  objectId: string
}): Promise<{
  isSuccess: boolean
  msg: string
}> => {
  const re = await post('/project/course/updateCourseComment', data).catch((err) => {
    return err.response || err
  })
  if (!re)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端操作失败')
    }
  if (!re.data)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端未返回结果')
    }
  return {
    isSuccess: re.data.success,
    msg: re.data.message
  }
}
export const delComment = async (
  objectId: string
): Promise<{
  isSuccess: boolean
  msg: string
}> => {
  const re = await post('/project/course/delCourseComment?objectId=' + objectId).catch((err) => {
    return err.response || err
  })
  if (!re)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端操作失败')
    }
  if (!re.data)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端未返回结果')
    }
  return {
    isSuccess: re.data.success,
    msg: re.data.message
  }
}
export const add = async (
  data: CourseDataModel
): Promise<{
  isSuccess: boolean
  msg: string
  data?: CourseDataModel
}> => {
  const re = await post('/project/course/addCourse', data).catch((err) => {
    return err.response || err
  })
  if (!re)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端操作失败')
    }
  if (!re.data)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端未返回结果')
    }
  return {
    isSuccess: re.data.success,
    msg: re.data.message,
    data: re.data.data || {}
  }
}
export const update = async (
  data: CourseDataModel
): Promise<{
  isSuccess: boolean
  msg: string
  data?: CourseDataModel
}> => {
  const re = await post('/project/course/updateCourse', data).catch((err) => {
    return err.response || err
  })
  if (!re)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端操作失败')
    }
  if (!re.data)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端未返回结果')
    }
  return {
    isSuccess: re.data.success,
    msg: re.data.message,
    data: re.data.data || {}
  }
}
export const doImport = async (
  file: any,
  onProgress?: (percentage: number) => void
): Promise<{
  isSuccess: boolean
  msg: string
}> => {
  const formData = new FormData()
  formData.append('file', file)
  const re = await upload('/project/course/uploadCourse', file, onProgress)
    .then((data) => {
      return data
    })
    .catch((error) => {
      return error.response || error
    })
  if (re && re.status === 200) {
    return {
      isSuccess: true,
      msg: '上传成功'
    }
  } else {
    return {
      isSuccess: false,
      msg: '上传失败'
    }
  }
}
export const activeCourse = async (id: string) => {
  const re = await post(`/project/course/enableCourse?objectId=${id}`).catch((err) => {
    return err.response || err
  })
  if (!re)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端操作失败')
    }
  if (!re.data)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端未返回结果')
    }
  return {
    isSuccess: re.data.success,
    msg: re.data.message,
    data: re.data.data || {}
  }
}
export const deactiveCourse = async (id: string) => {
  const re = await post(`/project/course/disableCourse?objectId=${id}`).catch((err) => {
    return err.response || err
  })
  if (!re)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端操作失败')
    }
  if (!re.data)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端未返回结果')
    }
  return {
    isSuccess: re.data.success,
    msg: re.data.message,
    data: re.data.data || {}
  }
}
export const hotCourse = async (id: string) => {
  const re = await post(`/project/course/hotCourse?objectId=${id}`).catch((err) => {
    return err.response || err
  })
  if (!re)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端操作失败')
    }
  if (!re.data)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端未返回结果')
    }
  return {
    isSuccess: re.data.success,
    msg: re.data.message,
    data: re.data.data || {}
  }
}
export const dehotCourse = async (id: string) => {
  const re = await post(`/project/course/unHotCourse?objectId=${id}`).catch((err) => {
    return err.response || err
  })
  if (!re)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端操作失败')
    }
  if (!re.data)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端未返回结果')
    }
  return {
    isSuccess: re.data.success,
    msg: re.data.message,
    data: re.data.data || {}
  }
}
