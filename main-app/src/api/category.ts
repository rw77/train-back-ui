import { get, post } from '@/axios/http'
import comFunc from '@/common/comFunc'

export const queryList = async (
  type: string
): Promise<{
  isSuccess: boolean
  msg: string
  data?: any[]
}> => {
  const re = await get(`/project/category/getCategoryListByType?type=${type}`).catch((err) => {
    return err.response || err
  })
  if (re.data) {
    return {
      isSuccess: re.data.success,
      msg: re.data.message,
      data: re.data.data || []
    }
  } else {
    return {
      isSuccess: false,
      msg: `查询失败:无返回结果`
    }
  }
}
/**
 * 获取select或级联select的options
 * @param type
 * @returns
 */
export const getCategoryOptions = async (
  type: CategoryType | ''
): Promise<CategorySelectOption[]> => {
  if (comFunc.nvl(type) === '') return []
  const re = await queryList(type)
  const rtn = [] as Array<CategorySelectOption>

  if (re.isSuccess) {
    let list = re.data!
    const fn = (parent: CategorySelectOption | null) => {
      let parentId = ''
      if (parent) parentId = parent.value
      let subs = list.filter((w) => comFunc.nvl(w.parentId) === parentId)
      if (subs) {
        subs.forEach((s) => {
          let item = {
            value: s.objectId,
            label: s.name,
            parentid: s.parentId,
            children: []
          }
          if (parent) {
            parent.children?.push(item)
          } else {
            rtn.push(item)
          }
          fn(item)
        })
      }
    }
    fn(null)
  }

  return rtn
}
/**
 * 获取级联词典表
 * @param type
 * @param separator
 * @returns
 */
export const getCategoryMap = async (
  type: CategoryType | '',
  separator: string = '/'
): Promise<Map<string, string>> => {
  if (comFunc.nvl(type) === '') return new Map()
  const re = await getCategoryOptions(type)
  const rtn = new Map()

  const fn = (parent: CategorySelectOption | null, parentLabel: string) => {
    if (parent && parent.children) {
      parent.children.forEach((item) => {
        if (!rtn.get(item.value)) {
          let label = `${parentLabel} ${separator} ${item.label}`
          rtn.set(item.value, label)
          fn(item, label)
        }
      })
    } else {
      re.forEach((item) => {
        let label = `${item.label}`
        rtn.set(item.value, label)
        fn(item, label)
      })
    }
  }

  fn(null, '')
  return rtn
}

export const addCategory = async (
  data: CategoryModel
): Promise<{
  isSuccess: boolean
  msg: string
  data?: CategoryModel[]
}> => {
  const re = await post('/project/category/addCategoryByType', data).catch((err) => {
    return err.response || err
  })
  if (re.data) {
    return {
      isSuccess: re.data.success,
      msg: re.data.message,
      data: re.data || []
    }
  } else {
    return {
      isSuccess: false,
      msg: `查询失败:无返回结果`
    }
  }
}
export const updateCategory = async (
  data: CategoryModel
): Promise<{
  isSuccess: boolean
  msg: string
  data?: CategoryModel[]
}> => {
  const re = await post('/project/category/updateCategory', data).catch((err) => {
    return err.response || err
  })
  if (re.data) {
    return {
      isSuccess: re.data.success,
      msg: re.data.message,
      data: re.data || []
    }
  } else {
    return {
      isSuccess: false,
      msg: `查询失败:无返回结果`
    }
  }
}
export const deleteCategory = async (
  objectId: string
): Promise<{
  isSuccess: boolean
  msg: string
}> => {
  const re = await get('/project/category/delCategoryById?objectId=' + objectId).catch((err) => {
    return err.response || err
  })
  if (re.data) {
    return {
      isSuccess: re.data.success,
      msg: re.data.message
    }
  } else {
    return {
      isSuccess: false,
      msg: `查询失败:无返回结果`
    }
  }
}
export const getTypeLabel = (code: string): string => {
  let re = CategoryTypeOptions.find((w) => w.code === code)
  if (re) return re.label
  else return ''
}
export const CategoryTypeOptions = [
  {
    code: 'TOPIC',
    label: '专题'
  },
  {
    code: 'COURSE',
    label: '课程'
  },
  {
    code: 'TRAIN',
    label: '培训'
  },
  {
    code: 'TEACH',
    label: '教研'
  },
  {
    code: 'NEWS',
    label: '新闻'
  }
]
