import { Sysinfo } from '@/common/config'
import { get, post } from '@/axios/http'
import { t } from '@/common/locale'

export const queryList = async (p: {
  categoryId?: string
  name?: string
  pageSize?: number
  to?: number
}): Promise<{
  isSuccess: boolean
  msg: string
  data?: any[]
  pageSize?: number
  pageNum?: number
  totalRow?: number
  totalPages?: number
}> => {
  if (!p.pageSize) p.pageSize = Sysinfo.PageSize
  if (!p.to) p.to = 1
  const re = await post('/project/topic/getTopicByCondition', {
    categoryId: p.categoryId,
    name: p.name,
    pageNum: p.to,
    pageSize: p.pageSize
  }).catch((err) => {
    return err.response || err
  })
  const data =
    re.data && re.data.data
      ? re.data.data
      : {
          list: [],
          pageSize: Sysinfo.PageSize,
          pageNum: 1,
          totalRow: 0,
          totalPages: 0
        }
  if (re.data) {
    if (re.data.success) {
      return {
        isSuccess: re.data.success,
        msg: re.data.message,
        data: data.list,
        pageSize: data.pageSize > 0 ? data.pageSize : Sysinfo.PageSize,
        pageNum: data.pageNum,
        totalRow: data.total,
        totalPages: data.pages
      }
    } else {
      return {
        isSuccess: re.data.success,
        msg: re.data.message
      }
    }
  } else {
    return {
      isSuccess: false,
      msg: `查询失败:无返回结果`
    }
  }
}
export const getTopicDetail = async (
  id: string
): Promise<{
  isSuccess: boolean
  msg: string
  data?: TopicDataModel
}> => {
  const re = await get('/project/topic/getTopicById?objectId=' + id).catch((err) => {
    return err.response || err
  })
  if (!re)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端操作失败')
    }
  if (!re.data)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端未返回结果')
    }
  return {
    isSuccess: re.data.success,
    msg: re.data.message,
    data: re.data.data || {}
  }
}
export const add = async (
  data: TopicDataModel
): Promise<{
  isSuccess: boolean
  msg: string
  data?: TopicDataModel
}> => {
  const re = await post('/project/topic/addTopic', data).catch((err) => {
    return err.response || err
  })
  if (!re)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端操作失败')
    }
  if (!re.data)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端未返回结果')
    }
  return {
    isSuccess: re.data.success,
    msg: re.data.message,
    data: re.data.data || {}
  }
}
export const update = async (
  data: TopicDataModel
): Promise<{
  isSuccess: boolean
  msg: string
  data?: TopicDataModel
}> => {
  const re = await post('/project/topic/updateTopic', data).catch((err) => {
    return err.response || err
  })
  if (!re)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端操作失败')
    }
  if (!re.data)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端未返回结果')
    }
  return {
    isSuccess: re.data.success,
    msg: re.data.message,
    data: re.data.data || {}
  }
}
export const deleteBy = async (
  data: TopicDataModel
): Promise<{
  isSuccess: boolean
  msg: string
}> => {
  const re = await get('/project/topic/deleteTopic?objectId=' + data.objectId).catch((err) => {
    return err.response || err
  })
  if (!re)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端操作失败')
    }
  if (!re.data)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端未返回结果')
    }
  return {
    isSuccess: re.data.success,
    msg: t(re.data.message || '操作失败：未知错误')
  }
}
export const getTopicCourses = async (
  topicId: string
): Promise<{
  isSuccess: boolean
  msg: string
  data?: any[]
}> => {
  const re = await get('/project/topic/getTopicCourseByTopicId?objectId=' + topicId).catch(
    (err) => err.response | err
  )
  if (!re)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端操作失败')
    }
  if (!re.data)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端未返回结果')
    }
  return {
    isSuccess: re.data.success,
    msg: re.data.message,
    data: re.data.data || []
  }
}
