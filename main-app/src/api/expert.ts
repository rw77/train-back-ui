import { Sysinfo } from '@/common/config'
import { get, post } from '@/axios/http'
import { t } from '@/common/locale'

export const queryList = async (p: {
  name?: string
  title?: string
  major?: string
  pageSize?: number
  to?: number
}): Promise<{
  isSuccess: boolean
  msg: string
  data?: any[]
  pageSize?: number
  pageNum?: number
  totalRow?: number
  totalPages?: number
}> => {
  if (!p.pageSize) p.pageSize = Sysinfo.PageSize
  if (!p.to) p.to = 1
  const re = await post('/project/expert/getExpertByCondition', {
    major: p.major === '' ? null : p.major,
    name: p.name === '' ? null : p.name,
    pageNum: p.to,
    pageSize: p.pageSize,
    title: p.title === '' ? null : p.title
  }).catch((err) => {
    console.error(err)
    return {
      isSuccess: false,
      msg: `查询失败:请联系管理员`
    }
  })
  const data =
    re.data && re.data.data
      ? re.data.data
      : {
          list: [],
          pageSize: Sysinfo.PageSize,
          pageNum: 1,
          totalRow: 0,
          totalPages: 0
        }
  if (re.data) {
    if (re.data.success) {
      return {
        isSuccess: re.data.success,
        msg: re.data.message,
        data: data.list,
        pageSize: data.pageSize > 0 ? data.pageSize : Sysinfo.PageSize,
        pageNum: data.pageNum,
        totalRow: data.total,
        totalPages: data.pages
      }
    } else {
      return {
        isSuccess: re.data.success,
        msg: re.data.message
      }
    }
  } else {
    return {
      isSuccess: false,
      msg: `查询失败:无返回结果`
    }
  }
}
export const getExpertDetail = async (
  id: string
): Promise<{
  isSuccess: boolean
  msg: string
  data?: ExpertDataModel
}> => {
  const re = await get('/project/expert/getExpertById?objectId=' + id).catch(
    (err) => err.response || err
  )

  if (!re)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端操作失败')
    }
  if (!re.data)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端未返回结果')
    }
  return {
    isSuccess: re.data.success,
    msg: re.data.message,
    data: re.data.data || {}
  }
}
export const add = async (
  data: ExpertDataModel
): Promise<{
  isSuccess: boolean
  msg: string
  data?: ExpertDataModel
}> => {
  const re = await post('/project/expert/addExpert', data).catch((err) => {
    return err.response || err
  })
  if (!re)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端操作失败')
    }
  if (!re.data)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端未返回结果')
    }
  return {
    isSuccess: re.data.success,
    msg: re.data.message,
    data: re.data.data || {}
  }
}
export const update = async (
  data: ExpertDataModel
): Promise<{
  isSuccess: boolean
  msg: string
  data?: ExpertDataModel
}> => {
  const re = await post('/project/expert/updateExpert', data)
  if (!re)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端操作失败')
    }
  if (!re.data)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端未返回结果')
    }
  return {
    isSuccess: re.data.success,
    msg: re.data.message,
    data: re.data.data || {}
  }
}
export const deleteBy = async (
  data: ExpertDataModel
): Promise<{
  isSuccess: boolean
  msg: string
}> => {
  const re = await get('/project/expert/delExpert?objectId=' + data.objectId).catch(
    (err) => err.response || err
  )
  if (!re)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端操作失败')
    }
  if (!re.data)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端未返回结果')
    }
  return {
    isSuccess: re.data.success,
    msg: re.data.message || '操作失败: 未知错误'
  }
}

export const getExpertPrizeDots = async (
  exportId: string
): Promise<{
  isSuccess: boolean
  msg: string
  data?: ExpertPrizeDot[]
}> => {
  const re = await get('/project/expert/getPrizeById?expertId=' + exportId).catch(
    (err) => err.response | err
  )
  if (!re)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端操作失败')
    }
  if (!re.data)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端未返回结果')
    }
  return {
    isSuccess: re.data.success,
    msg: re.data.message || '操作失败: 未知错误',
    data: re.data.data
  }
}
export const getExpertCourses = async (
  exportId: string
): Promise<{
  isSuccess: boolean
  msg: string
  data?: any[]
}> => {
  const re = await get('/project/expert/getCourseById?objectId=' + exportId).catch(
    (err) => err.response | err
  )
  if (!re)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端操作失败')
    }
  if (!re.data)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端未返回结果')
    }
  return {
    isSuccess: re.data.success,
    msg: re.data.message || '操作失败: 未知错误',
    data: re.data.data
  }
}
export const deleteExpertCourse = async (
  expertId: string,
  courseIds: string[]
): Promise<{
  isSuccess: boolean
  msg: string
}> => {
  const re = await post('/project/expert/delCourseByExpertIdAndCourseId', {
    expertId: expertId,
    courseIds: courseIds
  }).catch((err) => err.response || err)
  if (!re)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端操作失败')
    }
  if (!re.data)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端未返回结果')
    }
  return {
    isSuccess: re.data.success,
    msg: re.data.message || '操作失败: 未知错误'
  }
}
