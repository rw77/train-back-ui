import { getFile, upload, get } from '@/axios/http'
import { axiosGet } from '@/axios/ori_axios'
import comFunc from '@/common/comFunc'
import { ServerConfig } from '@/common/config'

export const previewFile = async (
  path: string
): Promise<{
  filetype: string
  content: string
}> => {
  let newfileurl = '/project/common/file/preview?filePath=' + path
  let re = await getFile(newfileurl)
  if (re && re.data) {
    return {
      filetype: re.data.fileType,
      content: re.data.content
    }
  } else {
    return {
      filetype: '',
      content: ''
    }
  }
}
/**
 * 上传文件到文件服务
 * @param {*} file 文件对象
 * @param {*} onProgress 进度事件,格式：onProgress(percentage)
 * @returns
 */
export const uploadFileStream = async (
  file: any,
  onProgress?: (percentage: number) => void
): Promise<{
  isSuccess: boolean
  remotePath?: string
  path?: string
}> => {
  const formData = new FormData()
  formData.append('file', file)
  const re = await upload('/project/common/file/upload', file, onProgress)
    .then((data) => {
      return data
    })
    .catch((error) => {
      return error.response || error
    })
  if (re && re.status === 200) {
    return {
      isSuccess: true,
      path: re.data,
      remotePath: `${ServerConfig.REMOTE_FILE_BASE}${re.data}`
    }
  } else {
    return {
      isSuccess: false
    }
  }
}
export const downloadFile = function (fileUrl: string, filename: string) {
  // 创建隐藏的可下载链接
  const eleLink = document.createElement('a')
  eleLink.download = filename
  eleLink.style.display = 'none'
  // 字符内容转变成地址
  eleLink.href = fileUrl
  // 触发点击
  document.body.appendChild(eleLink)
  eleLink.click()
  // 然后移除
  document.body.removeChild(eleLink)
}
export const deleteFile = async (
  path: string
): Promise<{
  isSuccess: boolean
  msg?: string
}> => {
  const re = await get('/project/common/file/delete?filePath=' + path).catch((error) => {
    return error.response || error
  })
  if (re && re.status === 200) {
    return {
      isSuccess: re.data.success,
      msg: re.data.message
    }
  } else {
    return {
      isSuccess: false,
      msg: '操作失败：服务端无结果返回'
    }
  }
}
/**
 * 上传视频到文件服务
 * @param {*} file 文件对象
 * @param {*} onProgress 进度事件,格式：onProgress(percentage)
 * @returns
 */
export const uploadVideo = async (
  file: any,
  fileName: string,
  onProgress?: (percentage: number) => void
): Promise<{
  isSuccess: boolean
  msg: string
  videoId: string
  path: string
  remotePath: string
}> => {
  const formData = new FormData()
  formData.append('file', file)
  const re = await upload(
    `/project/common/file/video/upload?fileName=${fileName}`,
    file,
    (percentage) => {
      if (onProgress) {
        onProgress(percentage / 2)
      }
    }
  )
    .then((data) => {
      return data
    })
    .catch((error) => {
      return error.response || error
    })
  console.log(re)
  if (re && re.status === 200) {
    let checkre = await axiosGet(re.data.metaurl)
    let size = file.size
    while (checkre && checkre.data.result !== 1) {
      if (onProgress) {
        onProgress(50 + Math.round((checkre.data.received * 100) / size / 2))
      }
      await comFunc.sleep(1000)
      checkre = await axiosGet(re.data.metaurl)
    }

    return {
      isSuccess: true,
      msg: '',
      videoId: re.data.ccvid,
      path: '',
      remotePath: ''
    }
  } else {
    return {
      isSuccess: false,
      msg: '上传失败',
      videoId: '',
      path: '',
      remotePath: ''
    }
  }
}
