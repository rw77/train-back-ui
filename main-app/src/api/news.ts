import { Sysinfo } from '@/common/config'
import { get, post } from '@/axios/http'
import { t } from '@/common/locale'
import comFunc from '@/common/comFunc'

export const queryList = async (p: {
  name?: string
  pageSize?: number
  to?: number
}): Promise<{
  isSuccess: boolean
  msg: string
  data?: any[]
  pageSize?: number
  pageNum?: number
  totalRow?: number
  totalPages?: number
}> => {
  if (!p.pageSize) p.pageSize = Sysinfo.PageSize
  if (!p.to) p.to = 1
  const re = await post('/project/news/getNewsByCondition', {
    name: comFunc.nvl(p.name) === '' ? null : p.name,
    pageNum: p.to,
    pageSize: p.pageSize
  }).catch((err) => {
    return err.response || err
  })
  const data =
    re.data && re.data.data
      ? re.data.data
      : {
          list: [],
          pageSize: Sysinfo.PageSize,
          pageNum: 1,
          totalRow: 0,
          totalPages: 0
        }
  if (re.data) {
    if (re.data.success) {
      return {
        isSuccess: re.data.success,
        msg: re.data.message,
        data: data.list,
        pageSize: data.pageSize > 0 ? data.pageSize : Sysinfo.PageSize,
        pageNum: data.pageNum,
        totalRow: data.total,
        totalPages: data.pages
      }
    } else {
      return {
        isSuccess: re.data.success,
        msg: re.data.message
      }
    }
  } else {
    return {
      isSuccess: false,
      msg: `查询失败:无返回结果`
    }
  }
}
export const getNewsDetail = async (
  id: string
): Promise<{
  isSuccess: boolean
  msg: string
  data?: NewsDataModel
}> => {
  const re = await get('/project/news/getNewsById?objectId=' + id).catch(
    (err) => err.response || err
  )
  if (!re)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端操作失败')
    }
  if (!re.data)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端未返回结果')
    }
  return {
    isSuccess: re.data.success,
    msg: re.data.message,
    data: re.data.data || {}
  }
}
export const add = async (
  data: NewsDataModel
): Promise<{
  isSuccess: boolean
  msg: string
  data?: NewsDataModel
}> => {
  const re = await post('/project/news/addNews', data).catch((err) => {
    return err.response || err
  })
  if (!re)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端操作失败')
    }
  if (!re.data)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端未返回结果')
    }
  return {
    isSuccess: re.data.success,
    msg: re.data.message,
    data: re.data.data || {}
  }
}
export const update = async (
  data: NewsDataModel
): Promise<{
  isSuccess: boolean
  msg: string
  data?: NewsDataModel
}> => {
  const re = await post('/project/news/updateNews', data).catch((err) => {
    return err.response || err
  })
  if (!re)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端操作失败')
    }
  if (!re.data)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端未返回结果')
    }
  return {
    isSuccess: re.data.success,
    msg: re.data.message,
    data: re.data.data || {}
  }
}
export const deleteBy = async (
  data: NewsDataModel
): Promise<{
  isSuccess: boolean
  msg: string
}> => {
  const re = await post('/project/news/delNews?objectId=' + data.objectId, {}).catch((err) => {
    return err.response || err
  })
  if (!re)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端操作失败')
    }
  if (!re.data)
    return {
      isSuccess: false,
      msg: t('操作失败：服务端未返回结果')
    }
  return {
    isSuccess: re.data.success,
    msg: re.data.message || '操作失败：未知错误'
  }
}
