declare type NewsQueryForm = {
  name?: string
}
declare type NewsDataModel = {
  title?: string
  description?: string
  content?: string
  objectId?: string
  originImg?: string
  thumbImg?: string
  subTitle?: string
}
