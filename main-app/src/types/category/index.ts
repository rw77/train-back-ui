declare type CategoryModel = {
  objectId?: string
  name?: string
  type?: string
  parentId?: string
}
declare type CategoryType = 'TOPIC' | 'COURSE' | 'TRAIN' | 'TEACH' | 'NEWS'
declare type CategorySelectOption = {
  value: string
  label: string
  parent?: string
  children?: CategorySelectOption[]
}

declare type CategoryQueryForm = {
  type: string
  name?: string
  parentId?: string
}
