declare type ExpertQueryForm = {
  name?: string
  major?: string
  title?: string
}
declare type ExpertPrizeDot = {
  description?: string
  expertId?: string
  name?: string
  objectId?: string
  originImg?: string
  thumbImg?: string
}
declare type ExpertDataModel = {
  introduction?: string
  major?: string
  name?: string
  objectId?: string
  originImg?: string
  thumbImg?: string
  title?: string
  courseIds?: string[]
  expertPrizeDtos?: ExpertPrizeDot[]
}
