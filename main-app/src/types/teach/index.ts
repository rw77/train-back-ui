declare type TeachDataModel = {
  beginTime?: string
  categoryId?: string
  description?: string
  endTime?: string
  flow?: string
  labelName?: string
  name?: string
  objectId?: string
  originImg?: string
  promoter?: string
  qrImg?: string
  question?: string
  speaker?: string
  sumary?: string
  sumaryFile?: string
  sumaryType?: string
  thumbImg?: string
  type?: string
}
