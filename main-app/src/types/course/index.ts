declare type CourseQueryForm = {
  name?: string
  categoryId?: string[]
}
declare type CourseDataModel = {
  categoryId?: string
  description?: string
  name?: string
  objectId?: string
  originImg?: string
  thumbImg?: string
  promoter?: string
  speaker?: string
  createTime?: string
  creater?: string
  joins?: number
  likes?: number
  updateTime?: string
  videoId?: string
  isEnable?: boolean
  isHot?: boolean
}
declare type CourseCommentModel = {
  objectId: string
  courseId?: string
  comment?: string
  status?: 'APPROVING' | 'APPROVED' | 'REJECTED'
  creater?: string
  createTime?: string
}
