declare type MenuDataModel = {
  function_no?: string
  function_name?: string
  function_url?: string
  p_function_no?: string
  functionlevel?: number
  action?: string
  sort_num?: number
  is_menu?: boolean
  target?: string
  url_type?: string
  icon?: string
}

declare type UserModel = {
  uid?: string
  loginid?: string
  loginname?: string
  userno?: string
  usertype?: string
  loginpass?: string
  mobile?: string
  qq?: string
  avatar?: string
  roles?: string[]
  roleId?: string[]
}
