declare type TopicQueryForm = {
  name?: string
  categoryId?: string
}
declare type TopicDataModel = {
  categoryId?: string
  description?: string
  name?: string
  objectId?: string
  originImg?: string
  thumbImg?: string
  courseIds?: string[]
}
