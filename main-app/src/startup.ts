import { useRequester } from '@/stores/index'
import type { App } from 'vue'
import { registerEnterDirective } from './directives/onEnter'

// 初始化http请求的相关状态参数
async function initHttpRequest() {
  const requestStore = useRequester()
  requestStore.$patch((state) => {
    state.isHttpEncrypt = false
  })
}
async function setGlobalProperties(app: App<Element>) {
  // 注册全局指令
  registerEnterDirective(app)
}

export const onStart = async (app: App<Element>) => {
  await setGlobalProperties(app)
  await initHttpRequest()
  return true
}
