import axios, { InternalAxiosRequestConfig } from 'axios'

const myinstance = axios.create()
myinstance.defaults.timeout = 240000
// 添加请求拦截器
myinstance.interceptors.request.use(
  (config: InternalAxiosRequestConfig) => {
    return config
  },
  (error) => {
    return Promise.reject(error)
  }
)

// 添加响应拦截器
myinstance.interceptors.response.use(
  function (response) {
    return response
  },
  function (error) {
    return Promise.reject(error)
  }
)
// post方法
export function axiosPost(url: string, data = {}, header = {}): Promise<any> {
  return new Promise((resolve, reject) => {
    myinstance({
      url: url,
      method: 'post',
      data: data,
      headers: header
    })
      .then((response) => {
        resolve(response)
      })
      .catch((error) => {
        reject(error)
      })
  })
}
// get方法
export function axiosGet(url: any, params = {}, header = {}): Promise<any> {
  return new Promise((resolve, reject) => {
    myinstance({
      url: url,
      method: 'get',
      params,
      headers: header
    })
      .then((response) => {
        resolve(response)
      })
      .catch((error) => {
        reject(error)
      })
  })
}
// delete方法
export function axiosDelete(url: string, params: any = {}): Promise<any> {
  let p: any = {}
  if (params.data) {
    p.data = params.data
  } else {
    p = params
  }
  return new Promise((resolve, reject) => {
    myinstance
      .delete(url, p)
      .then((response) => {
        resolve(response)
      })
      .catch((error) => {
        reject(error)
      })
  })
}
// patch方法
export function axiosPatch(url: string, data = {}, header = {}): Promise<any> {
  return new Promise((resolve, reject) => {
    myinstance({
      url: url,
      method: 'patch',
      data: data,
      headers: header
    })
      .then((response) => {
        resolve(response)
      })
      .catch((error) => {
        reject(error)
      })
  })
}
