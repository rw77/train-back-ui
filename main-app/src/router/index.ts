import { createRouter, createWebHistory, createWebHashHistory } from 'vue-router'
import ssHelper from '@/common/sessionStoreHelper'
import management from './management'
import { CURRENT_ROUTE_INFO_KEY, JWT_TOKEN_KEY, ROUTER_NAV_LIST_KEY } from '@/constants'
import busimanagement from './busimanagement'
import { Sysinfo } from '@/common/config'

const router = createRouter({
  history:
    Sysinfo.RouteMode === 'history'
      ? createWebHistory(import.meta.env.VITE_BASE_PATH)
      : createWebHashHistory(import.meta.env.VITE_BASE_PATH),
  routes: [
    {
      path: '/',
      name: 'main',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/Login.vue')
    },
    {
      path: '/dashboard',
      name: 'home',
      component: () => import('../views/Layout.vue'),
      children: [...management]
    },
    {
      path: '/service',
      name: 'service',
      component: () => import('../views/Layout.vue'),
      children: [...busimanagement]
    }
  ]
})
router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (ssHelper.get(JWT_TOKEN_KEY)) {
      const currentRouteInfo = ssHelper.get(CURRENT_ROUTE_INFO_KEY)
      const allMenuList: any[] = ssHelper.get(ROUTER_NAV_LIST_KEY, [])
      let showNavArray = currentRouteInfo?.navArray || [] // 最后存储菜单的数组
      let toRouterObj = {
        path: '',
        no: '',
        name: ''
      }
      // 判断当前菜单是否在功能列表中
      let hadMenuList = false
      // 通过所有的功能菜单,判断即将跳转的菜单是否在menu中
      for (let i = 0; i < allMenuList.length; i++) {
        if (allMenuList[i].function_url === to.fullPath) {
          toRouterObj.name = allMenuList[i].function_name
          toRouterObj.no = allMenuList[i].function_no
          toRouterObj.path = allMenuList[i].function_url
          hadMenuList = true
          break
        }
      }
      // 判断当前单开的菜单的数组中是否已经包含该菜单
      let hadOpen = false
      for (let i = 0; i < showNavArray.length; i++) {
        if (
          showNavArray[i].path === to.fullPath &&
          showNavArray[i].name === toRouterObj.name &&
          showNavArray[i].no === toRouterObj.no
        ) {
          hadOpen = true
          break
        }
      }
      if (!hadOpen) {
        // 没有添加就添加进入
        showNavArray.push(toRouterObj)
      }
      ssHelper.set(CURRENT_ROUTE_INFO_KEY, {
        currentRoute: hadMenuList ? to.fullPath : '/',
        navArray: showNavArray && showNavArray.length ? showNavArray : []
      })
      if (hadMenuList) {
        next()
      } else {
        next({
          path: '/',
          query: { redirect: to.fullPath }
        })
      }
    } else {
      ssHelper.set(CURRENT_ROUTE_INFO_KEY, {
        currentRoute: '/',
        navArray: []
      })
      next({
        path: '/',
        query: { redirect: to.fullPath }
      })
    }
  } else {
    next()
  }
})
router.afterEach((to, from) => {})

export default router
