export default [
  {
    path: 'home',
    component: () => import('../views/management/Dashboard.vue'),
    meta: { title: '首页', requiresAuth: true }
  },
  {
    path: 'userinfo',
    component: () => import('../views/management/UserCenter.vue'),
    meta: { title: '用户中心', requiresAuth: false }
  },
  {
    path: 'users',
    component: () => import('../views/management/users/index.vue'),
    meta: { title: '账号管理', requiresAuth: false }
  },
  {
    path: 'category',
    component: () => import('../views/category/index.vue'),
    meta: { title: '分类管理', requiresAuth: true }
  },
  {
    path: 'functionManagement',
    component: () => import('../views/management/Menu.vue'),
    meta: { title: '功能管理', requiresAuth: true }
  },
  {
    path: 'limitsManagement',
    component: () => import('../views/management/limitsManagement/LimitsManagement.vue'),
    meta: { title: '权限管理', requiresAuth: true }
  },

  {
    path: '/:pathMatch(.*)*',
    name: '404Page',
    component: () => import('../views/404.vue')
  }
]
