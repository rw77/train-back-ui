export default [
  {
    path: 'expert',
    component: () => import('../views/expert/index.vue'),
    meta: { title: '专家管理', requiresAuth: true }
  },
  {
    path: 'course',
    component: () => import('../views/course/index.vue'),
    meta: { title: '课程管理', requiresAuth: true }
  },
  {
    path: 'topic',
    component: () => import('../views/topic/index.vue'),
    meta: { title: '专题管理', requiresAuth: true }
  },
  {
    path: 'news',
    component: () => import('../views/news/index.vue'),
    meta: { title: '资讯管理', requiresAuth: true }
  },
  {
    path: 'teach',
    component: () => import('../views/teach/index.vue'),
    meta: { title: '教研管理', requiresAuth: true }
  },
  {
    path: 'train',
    component: () => import('../views/train/index.vue'),
    meta: { title: '培训管理', requiresAuth: true }
  },
  {
    path: 'user',
    component: () => import('../views/users/index.vue'),
    meta: { title: '用户管理', requiresAuth: true }
  },
  {
    path: '/:pathMatch(.*)*',
    name: '404Page',
    component: () => import('../views/404.vue')
  }
]
