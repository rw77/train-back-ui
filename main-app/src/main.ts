import { createApp } from 'vue'
import { createPinia } from 'pinia'
import router from './router'
import App from './App.vue'
import './assets/iconfont/iconfont.js'
import './assets/iconfont/iconfont.css'
import '@/styles/element/index.scss'

const app = createApp(App)
app.use(createPinia())
// 从服务端获取初始化信息
import('./startup').then((m) => {
  m.onStart(app)
})
app.use(router)
router.isReady().then(() => app.mount('#app'))
