import {useLocale} from '@/stores/index'
const uselocale = useLocale()

function t (key:string):string {
    return uselocale.t(key)
}
function setLocale(locale: string) {
    uselocale.setCurrentLocale(locale)
}

setLocale('zh-cn')

export {setLocale, t}

