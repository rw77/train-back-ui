import comFunc from './comFunc'

class SessionStorageHelper {
  constructor() {}
  /**
   * 获取sessiongstoreage中的数据
   * @param key
   * @param defaultValue 如果未获取数据则返回给定的默认值, 不给默认为空字符串
   * @returns JSON转化后的对象，如果无数据时，则返回空字符串或defaultValue
   */
  get = (key: string, defaultValue?: any) => {
    if (sessionStorage.getItem(key) && typeof sessionStorage.getItem(key) !== undefined) {
      try {
        return JSON.parse(comFunc.nvl(sessionStorage.getItem(key)))
      } catch {
        return sessionStorage.getItem(key)
      }
    } else {
      if (defaultValue) return defaultValue
      else return ''
    }
  }
  set = (key: string, v: any) => {
    if (v && typeof v !== undefined) {
      sessionStorage.setItem(key, JSON.stringify(v))
    }
  }
  clear = () => {
    sessionStorage.clear()
  }
  remove = (key: string) => {
    sessionStorage.removeItem(key)
  }
}

export default new SessionStorageHelper()
