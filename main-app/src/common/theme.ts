import { useAppSettings } from '@/stores/index'

export type ThemeColor = 'dark' | ''

const appsettings = useAppSettings()

export const changeTheme = (theme: ThemeColor) => {
  appsettings.changeThemeColor(theme)
  const html: any = document.querySelector('html')
  html.className = theme
}

export const initTheme = () => {
  changeTheme(appsettings.getThemeColor() as ThemeColor)
}

export const getCurrentTheme = () => {
  return appsettings.getThemeColor()
}

export const collapseMenu = () => {
  appsettings.setCollapseMenu(true)
}

export const expandMenu = () => {
  appsettings.setCollapseMenu(false)
}

export const isCollapseMenu = () => appsettings.isCollapseMenu()
