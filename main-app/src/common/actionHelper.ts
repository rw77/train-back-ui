import { ROUTER_NAV_LIST_KEY } from '@/constants'
import sessionStoreHelper from './sessionStoreHelper'

export const action = (action: string) => {
  const navlist = sessionStoreHelper.get(ROUTER_NAV_LIST_KEY, [])
  const curRouterPath = window.location.hash.substring(1)
  const item = navlist.find((w) => w.function_url === curRouterPath)
  const arr = item.actions.split(',')
  if (arr.find((w) => w === action)) return true
  else return false
}
