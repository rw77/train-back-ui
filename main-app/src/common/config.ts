import logo from '/src/assets/logo.svg'
import logoCollapse from '/src/assets/logo_collapse.svg'

//配置档的base路径，生产环境需要根据.env.production中的设置来改
const basePath = '/'
const fetchFile = await fetch(`${basePath}config.json`)
const configjson = await fetchFile.json()
const { $System, $URL, AliyunOSS } = configjson
const Sysinfo = {
  Title: '',
  Version: '',
  Logo: '',
  LogoCollapse: '',
  Copyright: '',
  ICP: '',
  ShowNotice: true,
  UseStaticMenu: false,
  PageSize: 10,
  PageSizeOptions: [10, 25, 50, 100],
  VideoUploadMaxSize: 100,
  PicUploadMaxSize: 2,
  RouteMode: 'hash'
}
Object.assign(Sysinfo, $System)

if (!Sysinfo.Logo) {
  Sysinfo.Logo = logo
}
if (!Sysinfo.LogoCollapse) {
  Sysinfo.LogoCollapse = logoCollapse
}
if (!Sysinfo.Copyright) {
  Sysinfo.Copyright = `Copyright ${new Date().getFullYear()} @EFFCYun.com`
}

const ServerConfig = {
  // 业务API
  CENTER_API: '',
  // 文件服务
  IMG_UPLOAD_URL: '',
  // 文件服务远程访问地址
  REMOTE_FILE_BASE: '',
  // 工作流服务
  WORKFLOW_API: '',
  // 应用Token
  ApplicationToken: '',
  // 应用授权码
  ApplicationCode: ''
}
if ($URL.CENTER_API) ServerConfig.CENTER_API = $URL.CENTER_API
if ($URL.IMG_UPLOAD_URL) ServerConfig.IMG_UPLOAD_URL = $URL.IMG_UPLOAD_URL
if ($URL.REMOTE_FILE_BASE) ServerConfig.REMOTE_FILE_BASE = $URL.REMOTE_FILE_BASE

const AliOssConfig = {
  // 桶名称
  bucket: '',
  // Oss控件上传文件存放路径
  Path: '',
  // 访问区域，与endpoint对应
  region: '',
  // 访问节点
  endpoint: '',
  // 刷新token的时间，单位分钟
  refreshTokenTime: 10
}
Object.assign(AliOssConfig, AliyunOSS)

export { Sysinfo, ServerConfig, AliOssConfig }
